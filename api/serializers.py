from rest_framework import serializers
from articles.models import Article , Comment
from users.models import CustomUser



class ArticleSerializer(serializers.ModelSerializer):
    class Meta:
        model= Article
        fields= ('id', 'author','title', 'body', 'date')


class CommentSerializer(serializers.ModelSerializer):
    class Meta:
        model= Comment
        fields= ('article', 'author', 'comment')


