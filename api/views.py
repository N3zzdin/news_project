from rest_framework import generics
from articles.models import Article, Comment
from .serializers import ArticleSerializer, CommentSerializer 
# Article API
class ListArticleApi(generics.ListCreateAPIView):
    queryset=Article.objects.all()
    serializer_class= ArticleSerializer

class DetialArticleApi(generics.RetrieveUpdateDestroyAPIView):
    queryset=Article.objects.all()
    serializer_class= ArticleSerializer
 #Comment API
class ListCommentApi(generics.ListCreateAPIView):
    queryset = Comment.objects.all()
    serializer_class= CommentSerializer

class DetialCommentApi(generics.RetrieveUpdateDestroyAPIView):
    queryset=Comment.objects.all()
    serializer_class= CommentSerializer




