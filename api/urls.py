from django.urls import path
from .views import ListArticleApi, DetialArticleApi, ListCommentApi, DetialCommentApi

urlpatterns= [
    path('com-list/<int:pk>/', DetialCommentApi.as_view()),
    path('com-list/', ListCommentApi.as_view()),
    path('<int:pk>/', DetialArticleApi.as_view()),
    path('', ListArticleApi.as_view()),
]